# Simple CSS Waves | Mobile & Full width

A Pen created on CodePen.io. Original URL: [https://codepen.io/jmbruges/pen/mdRprBm](https://codepen.io/jmbruges/pen/mdRprBm).

Lightweight animation between header & content. Easy to customize and apply into any website! Works with all devices and screen sizes.
